This repository contains a minimal setup for integrating
[dex](https://github.com/coreos/dex) and
[oauth2\_proxy](https://github.com/bitly/oauth2_proxy) into nginx for auth.


## Requirements:

- Vagrant 2.0.3 or newer
- VirtualBox
- Ansible


## Usage

```
$ vagrant up
```

Once this as completed, open your browser at <http://192.168.50.4>. You should
be greated by a login screen which accepts `test@team.com` and `team` as
credentials.
